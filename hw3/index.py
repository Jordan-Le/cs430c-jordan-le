"""
Name: Jordan Le
Date: 11/2/19
Hw 2: Boba Book Flask App
        - Create a basic flask app for a bubble tea version of yelp.
        - Use Sqlite3 backend and update fields accordingly.
Run with:
        - python3 app.py
"""

from flask import render_template
from flask.views import MethodView
import gbmodel

class Index(MethodView):
    def get(self):
        """
        Gets the model and loads its output into a list of dictionaries to populate index.html.
        :return: Rendered template of index.html
        """
        # Using the SQLite3 model
        model = gbmodel.get_model()
    
        # Creating a list of dictionaries containing all the data pulled from the gbmodel database
        # This includes: name, address, date, city, state, zipcode, hours, phonenum, rating, email, review, order
        entries = [dict(name=row[0], address=row[1], signed_on=row[2], city=row[3], state=row[4], 
                        zipcode=row[5], hours=row[6], phonenum=row[7], 
                        rating=row[8], email=row[9], message=row[10], 
                        order=row[11] ) for row in model.select()]

        # Return the updated flask html with the entries
        return render_template('index.html', entries=entries)
