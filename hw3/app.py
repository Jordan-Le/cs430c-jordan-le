"""
Name: Jordan Le
Date: 11/2/19
Hw 2: Boba Book Flask App
        - Create a basic flask app for a bubble tea version of yelp.
        - Use Sqlite3 backend and update fields accordingly.
Run with:
        - python3 app.py
"""

import flask
from flask.views import MethodView
from index import Index
from sign import Sign

app = flask.Flask(__name__)       # our Flask app

# Will allow app to use GET features from DB on index.html page
app.add_url_rule('/',
                 view_func=Index.as_view('index'),
                 methods=["GET"])

# Will allow app to use GET and POST features from DB on sign.html page
app.add_url_rule('/sign/',
                 view_func=Sign.as_view('sign'),
                 methods=['GET', 'POST'])

# Run the flask app on port 8000
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=8000)
