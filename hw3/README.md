# Hw 3 Objectives:

## Containerize Homework #2 using Ubuntu 18.04 as a base Linux image
    * Upload image to your Docker Hub account under the name "<dockerhub_id>/hw3large"
    * Repeat using a different base image (Alpine, Busybox, Minimal Ubuntu) in order to make it as *small* as possible
    * Upload it to your Docker Hub account under the name "<dockerhub_id>/hw3small"
    * Show (via screenshot) the size of both containers on Docker Hub by clicking on Tags
    * Remove all containers and container images on your system, then use docker run to instantiate each container directly from one of your images on Docker Hub
    * Show the console output displaying container image layers being pulled
    * Connect to your web app locally and show it works (wget or browser)


## Submission
    * Containers via Docker Hub (hw3large, hw3small)
    * Bitbucket submission of source files (including the two Dockerfiles used to build the containers), along with a single document with your screenshots in directory <your_cs430_repo>/hw3
    * D2L zipfile submission of hw3 for timestamp
    * Prize for the smallest 3 working containers submitted


## Run Large

### Large Set Up:
    `docker run -di -p 8000:8000 --name hw3l jor25/hw3large`

### Large Verify:
    `make verify_big`

### Large Clean Up:
    `make remove_big`


## Run Small

### Small Set Up:
    `docker run -di -p 8000:8000 --name hw3s jor25/hw3small`

### Small Verify:
    `make verify_small`

### Small Clean Up:
    `make remove_small`

