"""
    File derived from wuchang's lab2 v2.
    Deals with initializing backend database model.
"""

#model_backend = 'pylist'
model_backend = 'sqlite3'

if model_backend == 'sqlite3':
    from .model_sqlite3 import model
elif model_backend == 'pylist':
    from .model_pylist import model
else:
    raise ValueError("No appropriate databackend configured. ")

appmodel = model()

def get_model():
    """
    :return: Specified model backend
    """
    return appmodel
