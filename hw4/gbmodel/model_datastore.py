"""
Date: 11/14/19
The Boba Book Flask App - Hw4 version
Note(s):
    - Implementation derived from the simple guestbook flask app from lab2 V4
    - Data is stored in datastore that follows this pattern:
    +------------+------------------+------------+-----------+ ... +------------+
    | name       | address          | date       | state     |     | bobaorder  |
    +============+==================+============+===========+ ... +============+
    | Boba Shop1 | 1234 somethin dr | 2012-05-28 | OR        |     | good stuff |
    +------------+------------------+------------+-----------+ ... +------------+

This table can be created with the following:
    [entity['name'],entity['address'],entity['date'],entity['city'],
     entity['state'],entity['zipcode'],entity['hours'],entity['phonenum'],
     entity['rating'],entity['email'],entity['message'],entity['bobaorder']]
"""
# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from .Model import Model
from datetime import datetime
from google.cloud import datastore

def from_datastore(entity):
    """ Translates Datastore results into the format expected by the application.

        Datastore typically returns:
            [Entity{key: (kind, id), prop: val, ...}]

        This returns:
            [ name, address, date, city, state, zipcode, 
              hours, phonenum, rating, email, message, bobaorder]
            where all values are Python strings except date which is a Python datetime.
        Or: None

        :return: list or None
    """
    # Checks if there's no entity - give back none
    if not entity:
        return None

    # Checks if the entity object is an instance or subclass of the list class. If so, pop last element.
    if isinstance(entity, list):
        entity = entity.pop()
    
    # Give back the datastore entity with all the fields for HTML form
    return [entity['name'],entity['address'],entity['date'],entity['city'],
            entity['state'],entity['zipcode'],entity['hours'],entity['phonenum'],
            entity['rating'],entity['email'],entity['message'],entity['bobaorder']]


class model(Model):
    def __init__(self):
        """ Initialize the client to my project id.
            This is required to use the google cloud datastore.
        """
        self.client = datastore.Client('cs430-jordan-le')

    def select(self):
        """ Perform a query to google cloud datastore on the kind: 'boba_review'.
            Fetch a list of results from the datastore and return the entities as a list.
            :return: list
        """
        query = self.client.query(kind = 'boba_review')
        entities = list(map(from_datastore,query.fetch()))
        return entities

    def insert(self, name, address, city, state, zipcode, hours, phonenum, rating, email, message, bobaorder ):
        """
        Inserts user populated HTML form as entry into database
        :param name: String
        :param address: String
        :param city: String
        :param state: String
        :param zipcode: String
        :param hours: String
        :param phonenum: String
        :param rating: String
        :param email: String
        :param message: String
        :param bobaorder: String
        :return: True
        """

        # Dictionary of parameters passed in from HTML form
        params = {'name':name, 'address':address, 'date':datetime.today(), 'city':city, 'state':state, 
                  'zipcode':zipcode, 'hours':hours, 'phonenum':phonenum, 'rating':rating, 
                  'email':email, 'message':message, 'bobaorder':bobaorder}

        # Get a key from the kind and update datastores with the given params
        key = self.client.key('boba_review')
        rev = datastore.Entity(key)
        rev.update( params )
        self.client.put(rev)
        return True
