# Hw 4 Objective:

## Requirements: 
    * Adapt your app from Homework #2 to work on App Engine's
    * Flexible environment using Cloud Datastore instead of sqlite3
    * Deploy the app using "gcloud app deploy"
    * Leave it up for the TA to test
    * Note: You must create a new "Kind" for your app so as to not conflict with the Datastore entries from the labs


## Submission:
    * Commit your code to Bitbucket under directory hw4
    * Place all code and configuration files in repo
    * Submit a file called url.txt in your directory containing the URL that points to your deployed application


## Application Currently:
    * Follows MVP pattern
    * Has Default landing page - also displays all bubble tea stores
    * Has page for creating/inserting new bubble tea stores via HTML form
    * Listens on Port 8000
    * Contains an google cloud datastore base model
    * Python documentation via docstrings
    * Page supports:
        * name, street address, date, city, state, zipcode, hours, phonenum, rating, email, message, bobaorder
    * Data model class - google cloud datastore


## CSS Animation Tutorials:
    - /* Animation Citation Tutorial: https://www.youtube.com/watch?v=ETXba6QOzYg */
    - /* How to make squares into circles: https://codeburst.io/how-to-animate-using-css-27e04208ee8 */
    - /* Position relative to allow links to work: https://stackoverflow.com/questions/14255969/a-href-tag-not-working-due-to-z-index */


## How to Use:
    Go to the link:
        https://cs430-jordan-le.appspot.com


## Files Modified/Added:
| File Name | Comment |
| ------ | ------ |
| __init__.py | Made modifications to file to adjust to Google Cloud Datastore |
| model_datastore.py | Building off Wuchang's Lab #2 Guestbook V4 .py file, Kind = 'boba_review' |
| app.yaml | Directly using Wuchang's Lab #2 Guestbook V4 .yaml file |
| requirements.txt | Directly using Wuchang's Lab #2 Guestbook V4 .txt file |
| * .css files | Had to rename all css files due to some caching issue |
