# Hw 2 Objective:
## Create a toy Python Flask web application for viewing (and eventually submitting) bubble tea store locations
    * Development and submission within the BitBucket repository from HW #1, create a directory "hw2" for your application
    * Develop using git and Bitbucket
    * Ensure changes are committed frequently
    * When complete, submit a zip file for a timestamp onto D2L dropbox


## Application should ...
    * Follow an MVC/MVP pattern and support the following routes/views
    * Default landing page
    * Page for reading all bubble tea stores
    * Page for creating/inserting a new bubble tea store via an HTML form
    * Listen on port 8000 when called directly from python
    * Contain an abstract data model class that serves as the base class for specific model instantiations (sqlite3, Cloud Datastore, etc)
    * Abstract model should be documented via Docstrings including parameters and return values with their types
    * Abstract model should support individual fields that a bubble tea store would typically have (name, street address, city, state, zip code, store hours, phone number, rating, review, drink to order, etc.)
    * Contain a derived data model class (e.g. model_sqlite3) that supports creation and reading of stores via a sqlite3 database


## Application Currently:
    * Follows MVP pattern
    * Has Default landing page - also displays all bubble tea stores
    * Has page for creating/inserting new bubble tea stores via HTML form
    * Listens on Port 8000
    * Contains an sqlite3 base model
    * Python documentation via docstrings
    * Page supports:
        * name, street address, date, city, state, zipcode, hours, phonenum, rating, email, message, bobaorder
    * Data model class - model_sqlite3 supports creation and reading of stored values from sqlite3 database


## CSS Animation Tutorials:
    - /* Animation Citation Tutorial: https://www.youtube.com/watch?v=ETXba6QOzYg */
    - /* How to make squares into circles: https://codeburst.io/how-to-animate-using-css-27e04208ee8 */
    - /* Position relative to allow links to work: https://stackoverflow.com/questions/14255969/a-href-tag-not-working-due-to-z-index */


## How to Run:
    In the main directory run this for set up:
        `source env/bin/activate`
    
    Next run the following to launch server:
        `python3 app.py`


## Terminate:
    To shut down the site:
        `CTRL-C`

    To leave virtual env:
        `deactivate`
