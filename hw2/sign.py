"""
Name: Jordan Le
Date: 11/2/19
Hw 2: Boba Book Flask App
        - Create a basic flask app for a bubble tea version of yelp.
        - Use Sqlite3 backend and update fields accordingly.
Run with:
        - python3 app.py
"""

from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import gbmodel

class Sign(MethodView):
    def get(self):
        """
        Get the sign.html site
        :return: Rendered version of sign.html
        """
        return render_template('sign.html')

    def post(self):
        """
        Accepts POST requests, and processes the form;
        :return: Redirect to index when completed.
        """
        # Using the SQLite3 model
        model = gbmodel.get_model()
        
        # Passing values form html form to sqlite3 model for insertion.
        # Values include: name, address, city, state, zipcode, hours, phonenum, rating, email, review, order
        model.insert(request.form['name'], request.form['address'], request.form['city'], request.form['state'], 
                     request.form['zipcode'], request.form['hours'], request.form['phonenum'],
                     request.form['rating'], request.form['email'], request.form['message'],
                     request.form['bobaorder'] )
        return redirect(url_for('index'))
