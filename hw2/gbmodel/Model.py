"""
   File derived from Wuchang's lab 2.
   Object class that passes on to one of the children classes:
        ie: model_sqlite3.py
"""
class Model():
    def select(self):
        """
        Gets all entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    
    def insert(self, name, address, city, state, zipcode, hours, phonenum, rating, email, message, order):
        """
        Inserts entry into database
        :param name: String
        :param address: String
        :param city: String
        :param state: String
        :param zipcode: String
        :param hours: String
        :param phonenum: String
        :param rating: String
        :param email: String
        :param message: String
        :param bobaorder: String
        :return: none
        :raises: Database errors on connection and insertion
        """
        pass
