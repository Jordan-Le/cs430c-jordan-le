"""
The Boba Book Flask App
Note(s):
    - Implementation derived from the simple guestbook flask app from lab2.
    - Queries are spacing sensitive, so expect long lines for queries.
    - Data is stored in SQLite database that looks something like the following:
    +------------+------------------+------------+-----------+ ... +------------+
    | name       | address          | date       | state     |     | bobaorder  |
    +============+==================+============+===========+ ... +============+
    | Boba Shop1 | 1234 somethin dr | 2012-05-28 | OR        |     | good stuff |
    +------------+------------------+------------+-----------+ ... +------------+

This table can be created with the following SQL:
    "create table bobabook (name, address, date, city, state, zipcode, hours, phonenum, rating, email, message, bobaorder);"

"""


from .Model import Model
from datetime import date
import sqlite3
DB_FILE = 'entries.db'    # file for our Database

class model(Model):
    def __init__(self):
        """
        Initialize a SQLite3 database connection on the entries.db file.
        - Open the connection to the DB file then check tables.
        - If the entries.db file exists and already has the table bobabook, use it.
        - Otherwise create a new table called bobabook in the entries.db file.
        """
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            # If the table already exists, get everything.
            cursor.execute("select count(rowid) from bobabook")
        except sqlite3.OperationalError:
            # If we run into issues, make the table with these specific fields.
            cursor.execute("create table bobabook (name, address, date, city, state, zipcode, hours, phonenum, rating, email, message, bobaorder)")
        # Close connection to the DB.
        cursor.close()


    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, address, date, city, state, 
                           zipcode, hours, phonenum, rating, 
                           email, message, bobaorder
        :return: List of tuples containing all rows of database
        """
        # Connect to DB
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()

        # Get everything from the bobabook table
        cursor.execute("SELECT * FROM bobabook")
        return cursor.fetchall()


    def insert(self, name, address, city, state, zipcode, hours, phonenum, rating, email, message, bobaorder ):
        """
        Inserts user populated HTML form as entry into database
        :param name: String
        :param address: String
        :param city: String
        :param state: String
        :param zipcode: String
        :param hours: String
        :param phonenum: String
        :param rating: String
        :param email: String
        :param message: String
        :param bobaorder: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        # Dictionary of parameters passed in from HTML form
        params = {'name':name, 'address':address, 'date':date.today(), 'city':city, 'state':state, 
                  'zipcode':zipcode, 'hours':hours, 'phonenum':phonenum, 'rating':rating, 
                  'email':email, 'message':message, 'bobaorder':bobaorder}

        # Connect to database file
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()

        # Execute SQL query (insert), populate with dictionary of parameters.
        cursor.execute("insert into bobabook (name, address, date, city, state, zipcode, hours, phonenum, rating, email, message, bobaorder) VALUES (:name, :address, :date, :city, :state, :zipcode, :hours, :phonenum, :rating, :email, :message, :bobaorder)", params)

        # Save changes to database and close connection
        connection.commit()
        cursor.close()
        return True
